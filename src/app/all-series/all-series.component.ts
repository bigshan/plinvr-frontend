import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';

@Component({
  selector: 'app-all-series',
  templateUrl: './all-series.component.html',
  styleUrls: ['./all-series.component.css']
})
export class AllSeriesComponent implements OnInit {
  user: any;
  categories: any;
  categoryId: any = 0;
  category: any;
  allSeries: any;
  start: number = 0;
  limit: number = 30;
  total: number;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.categoryId = this.route.snapshot.paramMap.get("id");
    this.userService.init();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchCategories();
    if (parseInt(this.categoryId) > 0) {
      this.fetchOne();
      this.fetchSeriesByCategory();
      this.fetchCategoryTotal();
    }
    else {
      this.fetchSeries();
      this.fetchTotal();
    }
  }



  nextPage() {
    this.start = this.start + this.limit - 1;
    if (parseInt(this.categoryId) > 0) {
      this.fetchOne();
      this.fetchSeriesByCategory();
      this.fetchCategoryTotal();
    }
    else {
      this.fetchSeries();
      this.fetchTotal();
    }
  }

  previousPage() {
    this.start = this.start - this.limit + 1;
    console.log(this.start);
    if (parseInt(this.categoryId) > 0) {
      this.fetchOne();
      this.fetchSeriesByCategory();
      this.fetchCategoryTotal();
    }
    else {
      this.fetchSeries();
      this.fetchTotal();
    }
  }

  fetchTotal() {
    let url = `${Common.API_URL}series/total`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchCategoryTotal() {
    let url = `${Common.API_URL}series/total-category/${this.categoryId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }
  navigate(url) {
    this.router.navigateByUrl(url);
  }

  fetchCategories() {
    let url = `${Common.API_URL}category/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.categories = data.data;
          console.log(this.categories);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchOne() {
    let url = `${Common.API_URL}category/one/${this.categoryId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.category = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchSeriesByCategory() {
    let url = `${Common.API_URL}series/category/${this.categoryId}/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.allSeries = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchSeries() {
    let url = `${Common.API_URL}series/all/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.allSeries = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

}
