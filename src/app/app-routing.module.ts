import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { VrPreviewComponent } from './vr-preview/vr-preview.component';
import { CategoryComponent } from './category/category.component';
import { TagComponent } from './tag/tag.component';
import { SeriesComponent } from './series/series.component';
import { EpisodeComponent } from './episode/episode.component';
import { AllSeriesComponent } from './all-series/all-series.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchComponent } from './search/search.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { FullscreenComponent } from './fullscreen/fullscreen.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'vr-experience/:id',
    component: VrPreviewComponent
  },
  {
    path: 'category/:id',
    component: CategoryComponent
  },
  {
    path: 'tag/:id',
    component: TagComponent
  },
  {
    path: 'series/:id',
    component: SeriesComponent
  },
  {
    path: 'episode/:id',
    component: EpisodeComponent
  },
  {
    path: 'all-series/:id',
    component: AllSeriesComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'favourite',
    component: FavouriteComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'fullscreen',
    component: FullscreenComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
