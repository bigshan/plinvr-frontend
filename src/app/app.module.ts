import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ModalComponent } from './modal/modal.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { VrPreviewComponent } from './vr-preview/vr-preview.component';
import { CategoryComponent } from './category/category.component';
import { SeriesComponent } from './series/series.component';
import { EpisodeComponent } from './episode/episode.component';
import { AllSeriesComponent } from './all-series/all-series.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchComponent } from './search/search.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { ImgFallbackDirective } from './img-fallback.directive';
import { FullscreenComponent } from './fullscreen/fullscreen.component';
import { TagComponent } from './tag/tag.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    ForgotPasswordComponent,
    ModalComponent,
    SidebarComponent,
    FooterComponent,
    HeaderComponent,
    VrPreviewComponent,
    CategoryComponent,
    SeriesComponent,
    EpisodeComponent,
    AllSeriesComponent,
    ProfileComponent,
    SearchComponent,
    FavouriteComponent,
    ImgFallbackDirective,
    FullscreenComponent,
    TagComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
