import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  user: any;
  categories: any;
  categoryName: any = 0;
  category: any;
  vrs: any;
  start: number = 0;
  limit: number = 10;
  total: number;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.categoryName = this.route.snapshot.paramMap.get("id");
    this.userService.init();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    if (this.categoryName != "all") {
      this.fetchOne();
    }
    else {
      this.fetchVr();
      this.fetchTotal();
    }

  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }


  fetchTotal() {
    let url = `${Common.API_URL}vr/total`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchCategoryTotal() {
    let url = `${Common.API_URL}vr/total-category/${this.category.id}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchOne() {
    let url = `${Common.API_URL}category/name/${this.categoryName}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.category = data.data;
          this.fetchVrByCategory();
          this.fetchCategoryTotal();
        }
      }, error => {
        console.log(error);
      });
  }

  nextPage() {
    this.start = this.start + this.limit - 1;
    if (this.categoryName != "all") {
      this.fetchOne();
      this.fetchVrByCategory();
      this.fetchCategoryTotal();
    }
    else {
      this.fetchVr();
      this.fetchTotal();
    }
  }

  previousPage() {
    this.start = this.start - this.limit + 1;
    console.log(this.start);
    if (this.categoryName != "all") {
      this.fetchOne();
      this.fetchVrByCategory();
      this.fetchCategoryTotal();
    }
    else {
      this.fetchVr();
      this.fetchTotal();
    }
  }
  singlePage(vr) {
    if (vr.series_id > 0) {
      this.navigate('episode/' + vr.id);
    } else {
      this.navigate('vr-experience/' + vr.id);
    }

  }

  fetchVrByCategory() {
    let url = `${Common.API_URL}vr/category/${this.category.id}/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  tagsToArray(str) {
    return str.split(", ");
  }

  fetchVr() {
    let url = `${Common.API_URL}vr/all/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

}
