import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';


@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  user: any;
  categories: any;
  category: any;
  favourites: any;
  start: number = 0;
  limit: number = 30;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.userService.init();
    this.user = this.userService.getUser();
    if(!this.user){
      this.navigate('');
    }
    this.fetchCategories();
    this.fetchFavourites();
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }

  fetchCategories() {
    let url = `${Common.API_URL}category/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.categories = data.data;
          console.log(this.categories);
        }
      }, error => {
        console.log(error);
      });
  }

  tagsToArray(str) {
    return str.split(", ");
  }

  singlePage(vr) {
    if (vr.series_id > 0) {
      this.navigate('episode/' + vr.id);
    } else {
      this.navigate('vr-experience/' + vr.id);
    }
  }

  fetchFavourites() {
    let url = `${Common.API_URL}vr-favourite/all/${this.user.id}/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.favourites = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

}
