import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fullscreen',
  templateUrl: './fullscreen.component.html',
  styleUrls: ['./fullscreen.component.css']
})
export class FullscreenComponent implements OnInit {
  saveVideoURL: any;
  vr: any;
  constructor(public sanitizer: DomSanitizer, private userService: UserServiceService, private router: Router) { }

  ngOnInit() {
    this.vr = this.userService.getVR();
    this.saveVideoURL = this.safeURL(this.vr.absolute_file_path);
  }

  safeURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }

  singlePage(vr) {
    if (vr.series_id > 0) {
      this.navigate('episode/' + vr.id);
    } else {
      this.navigate('vr-preview/' + vr.id);
    }

  }
}
