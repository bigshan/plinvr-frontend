import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { Router, NavigationEnd } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Common } from '../common';
import { externalMethods } from '../../assets/js/external';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  user: any;
  categories: any;
  query: any;
  previewCategory: any;
  previewType: any;
  allSeries: any;

  constructor(private router: Router, public http: HttpClient, private userService: UserServiceService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.userService.init();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchCategories();
    this.fetchSeries();
  }

  ngAfterViewInit() {
    externalMethods();
  }

  logout() {
    this.userService.clearUser();
    this.user = null;
  }

  search() {
    this.userService.setQuery(this.query);
    this.navigate('search');
  }

  navigate(url) {
    console.log("hi");
    this.router.navigateByUrl(url);
  }

  changeCategory(category, type) {
    this.previewCategory = category;
    this.previewType = type;
  }
  fetchCategories() {
    let url = `${Common.API_URL}category/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.categories = data.data;
          this.previewCategory = this.categories[0];
          console.log(this.categories);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchSeries() {
    let url = `${Common.API_URL}series/all/0/5`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.allSeries = data.data;

        }
      }, error => {
        console.log(error);
      });
  }

}
