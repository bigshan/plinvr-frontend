import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  vrs: any = [];
  featuredVRs: any = [];
  popularVrs: any = [];
  todayVrs: any = [];
  olderVrs: any = [];
  series: any = null;
  allSeries: any = [];
  all2ndSeries: any = [];
  user: any;
  showSplash: boolean = true;
  comingSoon: any;

  constructor(private router: Router, public http: HttpClient, private userService: UserServiceService) { }

  ngOnInit() {
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchFeaturedVRs();
    //this.fetchVrs();
    this.fetchPopular();
    this.fetchToday();
    this.fetchOlder();
    this.fetchSeriesLatest();
    this.fetchfirstFour();
    this.fetchSecondSix();
    this.fetchComingSoon();
  }

  fetchFeaturedVRs() {
    let url = `${Common.API_URL}vr/featured`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.featuredVRs = data.data;
          this.showSplash = false;

          window.setTimeout(function () {
            // initialization of slick carousel
            $('.js-slick-carousel').each(function () {
              var slickCarousel = $.HSCore.components.HSSlickCarousel.init($(this));
            });
          }, 200);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchComingSoon() {
    let url = `${Common.API_URL}vr/coming-soon`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.comingSoon = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchVrs() {
    let url = `${Common.API_URL}vr/all/0/8`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
          this.showSplash = false;

          window.setTimeout(function () {
            // initialization of slick carousel
            $('.js-slick-carousel').each(function () {
              var slickCarousel = $.HSCore.components.HSSlickCarousel.init($(this));
            });
          }, 200);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchfirstFour() {
    let url = `${Common.API_URL}series/all/0/4`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.allSeries = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  addFavourite(VRId) {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "user_id": this.user.id,
      "vr_id": VRId
    }

    let url = `${Common.API_URL}vr-favourite/add`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response;
      }, error => {
        console.log(error);
      });
  }

  fetchSecondSix() {
    let url = `${Common.API_URL}series/all/3/6`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.all2ndSeries = data.data;
        }
      }, error => {
        console.log(error);
      });
  }
  
  tagsToArray(str) {
    return str.split(", ");
  }

  fetchPopular() {
    let url = `${Common.API_URL}vr/popular`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.popularVrs = data.data;

        }
      }, error => {
        console.log(error);
      });
  }

  fetchToday() {
    let url = `${Common.API_URL}vr/today`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.todayVrs = data.data;

        }
      }, error => {
        console.log(error);
      });
  }

  fetchOlder() {
    let url = `${Common.API_URL}vr/older`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.olderVrs = data.data;

        }
      }, error => {
        console.log(error);
      });
  }

  fetchSeriesLatest() {
    let url = `${Common.API_URL}series/latest`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.series = data.data;
          console.log(this.series);
        }
      }, error => {
        console.log(error);
      });
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }
}
