import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';
import { UserServiceService } from '../user-service.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  user: any;
  email: string;
  password: string;
  message: string;

  constructor(private router: Router, public http: HttpClient, private userService: UserServiceService) { }

  ngOnInit() {
    this.userService.init();
  }

  successRegister(user) {
    $("#loginModal").modal('hide');
  }

  submit() {

    if (this.email.trim().length <= 0) {
      return this.message = "Email is required"
    }

    if (this.password.trim().length <= 0) {
      return this.message = "password is required"
    }

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "email": this.email,
      "password": this.password
    }

    let url = `${Common.API_URL}user/login`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.user = data.data;
          this.userService.setUser(this.user);
          $("#loginModal").modal('hide');
        }
        else {
          this.message = data.message;
        }
      }, error => {
        console.log(error);
      });
  }
}
