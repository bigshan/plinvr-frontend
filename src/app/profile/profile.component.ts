import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: any;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.userService.init();
    this.user = this.userService.getUser();
    console.log(this.user);
    if(!this.user){
      this.navigate('');
    }
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
  }

  logout() {
    this.userService.clearUser();
    this.user = null;
    this.navigate('');
  }

  navigate(url) {
    console.log("hi");
    this.router.navigateByUrl(url);
  }

  handleFileSelect() {
    var img: any = document.querySelector('#profile-image');
    var file: any = document.querySelector('#file-input');
    var userId = this.user.id;
    img.src = URL.createObjectURL(file.files[0]); // set src to blob url
    var reader = new FileReader();
    var http: HttpClient = this.http;
    var userService = this.userService;
    // Closure to capture the file information.
    reader.onload = (function (theFile) {
      return function (e) {
        var binaryData = e.target.result;
        // Converting Binary Data to base 64
        this.base64 = window.btoa(binaryData);
        // showing file converted to base64
        var headers = new HttpHeaders();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        const requestOptions = { headers: headers };

        let userData = {
          "id": userId,
          "photo": this.base64
        }
        let url = `${Common.API_URL}user/update-photo`;
        http.post(url, userData, requestOptions)
          .subscribe(response => {
            console.log(response);
            let data: any = response
            if (parseInt(data.code) == 1) {
              this.user = data.data;
              userService.setUser(this.user);
            }
            else {
              this.message = data.message;
            }
          }, error => {
            console.log(error);
          });
      };

    })(file.files[0]);

    // Read in the image file as a data URL.
    reader.readAsBinaryString(file.files[0]);

  }
}
