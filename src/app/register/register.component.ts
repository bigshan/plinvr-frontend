import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: any;
  lastname: string;
  firstname: string;
  email: string;
  password: string;
  confirm: string;
  message: string;

  @Output() success: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, public http: HttpClient, private userService: UserServiceService) { }

  ngOnInit() {
    this.userService.init();
  }

  submit() {

    if (!this.lastname || this.lastname.trim().length <= 0) {
      return this.message = "Last Name is required"
    }

    if (!this.firstname || this.firstname.trim().length <= 0) {
      return this.message = "First Name is required"
    }

    if (!this.email || this.email.trim().length <= 0) {
      return this.message = "Email is required"
    }

    if (!this.password || this.password.trim().length <= 0) {
      return this.message = "password is required"
    }

    if (this.password != this.confirm) {
      return this.message = "password does not match"
    }

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "lastname": this.lastname,
      "firstname": this.firstname,
      "email": this.email,
      "password": this.password
    }

    let url = `${Common.API_URL}user/register`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.user = data.data;
          this.userService.setUser(this.user);
          this.success.emit(this.user);
        }
        else {
          this.message = data.message;
        }
      }, error => {
        console.log(error);
      });
  }

}
