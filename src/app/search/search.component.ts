import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  query;
  categories: any;
  vrs: any;
  start: number = 0;
  limit: number = 30;
  total: number;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.query = this.userService.getQuery();
    this.userService.init();

    this.fetchCategories();
    this.fetchVr();
    this.fetchTotal();
  }

  nextPage() {
    this.start = this.start + this.limit - 1;
    this.fetchVr();
    this.fetchTotal();
  }

  previousPage() {
    this.start = this.start - this.limit + 1;
    this.fetchVr();
    this.fetchTotal();
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }

  fetchCategories() {
    let url = `${Common.API_URL}category/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.categories = data.data;
          console.log(this.categories);
        }
      }, error => {
        console.log(error);
      });
  }


  fetchVr() {
    let queryURL = this.query.replace(" ", "+");
    let url = `${Common.API_URL}vr/search/${queryURL}/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchTotal() {
    let queryURL = this.query.replace(" ", "+");
    let url = `${Common.API_URL}vr/total-search/${queryURL}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }

  tagsToArray(str) {
    return str.split(", ");
  }
  
  singlePage(vr) {
    if (vr.series_id > 0) {
      this.navigate('episode/' + vr.id);
    } else {
      this.navigate('vr-experience/' + vr.id);
    }

  }
}
