import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';


@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
  seriesId: any;
  series: any;
  user: any;
  otherSeries: any;
  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.seriesId = this.route.snapshot.paramMap.get("id");
    this.userService.init();
    this.user = this.userService.getUser();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchOne();
  }

  fetchOne() {
    let url = `${Common.API_URL}series/one/${this.seriesId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.series = data.data;
          console.log(this.series);
          this.fetchOthers();
        }
      }, error => {
        console.log(error);
      });
  }

  fetchOthers() {
    let url = `${Common.API_URL}series/other/${this.series.id}/${this.series.category_id}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.otherSeries = data.data;
          console.log(this.otherSeries)
        }
      }, error => {
        console.log(error);
      });
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }
}
