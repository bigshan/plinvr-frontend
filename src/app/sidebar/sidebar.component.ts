import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  categories: any;
  tags: any;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.fetchCategories();
    this.fetchTags();
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }

  fetchCategories() {
    let url = `${Common.API_URL}category/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.categories = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchTags() {
    let url = `${Common.API_URL}tag/all`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.tags = data.data;
        }
      }, error => {
        console.log(error);
      });
  }


}
