import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';


@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {


  user: any;
  categories: any;
  tagName: any = 0;
  tag: any;
  vrs: any;
  start: number = 0;
  limit: number = 10;
  total: number;

  constructor(public http: HttpClient, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.tagName = this.route.snapshot.paramMap.get("id");
    this.userService.init();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchOne();

  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }




  fetchTagTotal() {
    let url = `${Common.API_URL}vr/total-tag/${this.tagName}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.total = data.data;
          console.log(this.total);
        }
      }, error => {
        console.log(error);
      });
  }

  fetchOne() {
    let url = `${Common.API_URL}tag/name/${this.tagName}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.tag = data.data;
          this.fetchVrByTag();
          this.fetchTagTotal();
        }
      }, error => {
        console.log(error);
      });
  }

  nextPage() {
    this.start = this.start + this.limit - 1;
    this.fetchOne();
    this.fetchVrByTag();
    this.fetchTagTotal();
  }

  previousPage() {
    this.start = this.start - this.limit + 1;
    console.log(this.start);
    this.fetchOne();
    this.fetchVrByTag();
    this.fetchTagTotal();
  }

  singlePage(vr) {
    if (vr.series_id > 0) {
      this.navigate('episode/' + vr.id);
    } else {
      this.navigate('vr-experience/' + vr.id);
    }

  }

  tagsToArray(str) {
    return str.split(", ");
  }
  
  fetchVrByTag() {
    let url = `${Common.API_URL}vr/tag/${this.tagName}/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchVr() {
    let url = `${Common.API_URL}vr/all/${this.start}/${this.limit}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = data.data;
        }
      }, error => {
        console.log(error);
      });
  }
}
