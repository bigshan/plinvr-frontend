import { Inject, Injectable, EventEmitter } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Subject } from 'rxjs';

const STORAGE_KEY = 'local_user';
const STORAGE_KEY1 = 'local_query';
const STORAGE_KEY2 = 'local_vr';

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {
  user: any = null;
  query: any = null;
  vr: any = null;

  userChange: Subject<any> = new Subject<any>();

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

  init() {
    this.user = this.storage.get(STORAGE_KEY) || null;
    this.query = this.storage.get(STORAGE_KEY1) || null;
    this.vr = this.storage.get(STORAGE_KEY2) || null;
    this.userChange.next(this.user);
  }

  getUser() {
    return this.user;
  }

  getQuery() {
    return this.query;
  }

  getVR() {
    return this.vr;
  }

  setUser(user) {
    this.user = user;
    this.userChange.next(this.user);
    this.storage.set(STORAGE_KEY, user);
  }

  setQuery(query) {
    this.query = query;
    this.storage.set(STORAGE_KEY1, query);
  }

  setVR(vr) {
    this.vr = vr;
    this.storage.set(STORAGE_KEY2, vr);
  }

  clearUser() {
    this.user = null;
    this.storage.set(STORAGE_KEY, this.user);
  }
}
