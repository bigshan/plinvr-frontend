import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrPreviewComponent } from './vr-preview.component';

describe('VrPreviewComponent', () => {
  let component: VrPreviewComponent;
  let fixture: ComponentFixture<VrPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
