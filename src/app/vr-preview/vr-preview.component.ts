import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../common';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';

@Component({
  selector: 'app-vr-preview',
  templateUrl: './vr-preview.component.html',
  styleUrls: ['./vr-preview.component.css']
})
export class VrPreviewComponent implements OnInit {

  vrId: any;
  vr: any;
  vrs: any;
  user: any;
  saveVideoURL: any;
  comment: string;
  message: string;
  comments: any;
  showCommentForm: boolean = true;
  isFavourite: boolean = false;
  loadingSpinner: boolean = true;
  isFullscreen = false;

  constructor(public http: HttpClient, public sanitizer: DomSanitizer, private userService: UserServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.vrId = this.route.snapshot.paramMap.get("id");
    this.userService.init();
    this.user = this.userService.getUser();
    this.userService.userChange.subscribe((value) => {
      this.user = value
    });
    this.fetchOne();
    this.fetchVRComments();
    if (this.user) {
      this.fetchFavourite();
    }

  }

  tagsToArray(str) {
    return str.split(", ");
  }
  
  navigate(url) {
    this.router.navigateByUrl(url);
  }

  fullscreen(vr) {
    this.userService.setVR(vr);
    this.navigate('fullscreen');
  }

  fetchOne() {
    let url = `${Common.API_URL}vr/view/${this.vrId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vr = data.data;
          this.saveVideoURL = this.safeURL(this.vr.absolute_file_path);
          this.fetchOthers();
        }
      }, error => {
        console.log(error);
      });
  }

  fetchOthers() {
    let url = `${Common.API_URL}vr/category/${this.vr.category_id}/0/10`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.vrs = [];
          let vrsResponse = data.data;
          for (var i = 0; i < vrsResponse.length; i++) {
            if (vrsResponse[i].id != this.vrId) {
              this.vrs.push(vrsResponse[i])
            }
          }
        }
      }, error => {
        console.log(error);
      });
  }

  fetchFavourite() {
    let url = `${Common.API_URL}vr-favourite/one/${this.user.id}/${this.vrId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.isFavourite = true;
        }
      }, error => {
        console.log(error);
      });
  }

  fetchVRComments() {
    let url = `${Common.API_URL}vr-comment/vr/${this.vrId}`;
    this.http.get(url)
      .subscribe(response => {
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.comments = data.data;
        }
      }, error => {
        console.log(error);
      });
  }


  safeURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  submit() {

    if (this.comment.trim().length <= 0) {
      return this.message = "Email is required"
    }

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "comment": this.comment,
      "user_id": this.user.id,
      "vr_id": this.vrId,
      "status": 1
    }

    let url = `${Common.API_URL}vr-comment/add`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.fetchVRComments();
          this.showCommentForm = false;
        }
        else {
          this.message = data.message;
        }
      }, error => {
        console.log(error);
      });
  }

  addFavourite() {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "user_id": this.user.id,
      "vr_id": this.vrId
    }

    let url = `${Common.API_URL}vr-favourite/add`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.isFavourite = true;
        }
      }, error => {
        console.log(error);
      });
  }

  removeFavourite() {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const requestOptions = { headers: headers };

    let userData = {
      "user_id": this.user.id,
      "vr_id": this.vrId
    }

    let url = `${Common.API_URL}vr-favourite/remove`;
    this.http.post(url, userData, requestOptions)
      .subscribe(response => {
        console.log(response);
        let data: any = response
        if (parseInt(data.code) == 1) {
          this.isFavourite = false;
        }
      }, error => {
        console.log(error);
      });
  }

}
