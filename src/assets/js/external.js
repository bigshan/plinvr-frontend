export function externalMethods() {
    console.log('Calling test 1 function');

    function customLoading() {

        try {
            // initialization of header
            var header = new HSHeader($('#header')).init();
        } catch (ex) {
            console.log(ex);
        }
        try {
            // initialization of mega menu

            var megaMenu = !megaMenu ? new HSMegaMenu($('.js-mega-menu'), {
                desktop: {
                    position: 'left'
                }
            }).init() : null;
            console.log(megaMenu);
        } catch (ex) {
            console.log(ex)
        }
        try {
            // initialization of fancybox
            $('.js-fancybox').each(function() {
                var fancybox = $.HSCore.components.HSFancyBox.init($(this));
            });
        } catch (ex) {}
        try {
            // initialization of unfold
            var unfold = new HSUnfold('.js-hs-unfold-invoker').init();
        } catch (ex) {}
        try {

            // initialization of form validation
            $('.js-validate').each(function() {
                $.HSCore.components.HSValidation.init($(this), {
                    rules: {
                        confirmPassword: {
                            equalTo: '#signupPassword'
                        }
                    }
                });
            });
        } catch (ex) {}
        try {
            // initialization of show animations
            $('.js-animation-link').each(function() {
                var showAnimation = new HSShowAnimation($(this)).init();
            });

        } catch (ex) {}
        try {

            // initialization of counter
            $('.js-counter').each(function() {
                var counter = new HSCounter($(this)).init();
            });

        } catch (ex) {}
        try {
            // initialization of sticky block
            var cbpStickyFilter = new HSStickyBlock($('#cbpStickyFilter'));
        } catch (ex) {}
        try {
            // initialization of cubeportfolio
            $('.cbp').each(function() {
                var cbp = $.HSCore.components.HSCubeportfolio.init($(this), {
                    layoutMode: 'grid',
                    filters: '#filterControls',
                    displayTypeSpeed: 0
                });
            });
        } catch (ex) {}
        try {
            $('.cbp').on('initComplete.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();

                // initialization of aos
                AOS.init({
                    duration: 650,
                    once: true
                });
            });
        } catch (ex) {}
        try {
            $('.cbp').on('filterComplete.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();

                // initialization of aos
                AOS.init({
                    duration: 650,
                    once: true
                });
            });

        } catch (ex) {}
        try {
            $('.cbp').on('pluginResize.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();
            });
        } catch (ex) {}
        try {
            // animated scroll to cbp container
            $('#cbpStickyFilter').on('click', '.cbp-filter-item', function(e) {
                $('html, body').stop().animate({
                    scrollTop: $('#demoExamplesSection').offset().top
                }, 200);
            });
        } catch (ex) {}
        try { // initialization of go to
            $('.js-go-to').each(function() {
                var goTo = new HSGoTo($(this)).init();
            });
        } catch (ex) {}

    }

    customLoading();

}